<?php
/**
 * Created by PhpStorm.
 * User: Lixing
 * Date: 19/2/18
 * Time: 9:01 PM
 */
use ToyRobot\RunRobot;

require __DIR__.'/vendor/autoload.php';

$robotApp = new RunRobot($argv);