<?php

namespace ToyRobot;

class Robot
{

    private $table = null;
    private $aspect = null;
    private $location = null;
    private $compass = ['NORTH', 'EAST', 'SOUTH', 'WEST'];

    /**
     * Robot constructor.
     *
     * @param $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;

        $this->location = ['x' => null, 'y' => null];
    }

    public function report()
    {
        if ($this->hasBeenPlaced()) {
            $output = 'Output: '
                      . $this->location['x'] . ', '
                      . $this->location['y'] . ', '
                      . $this->compass[$this->aspect];

            return $output;
        } else {
            return '';
        }
    }

    private function hasBeenPlaced()
    {
        $anyNull = $this->location['x'] === null ||
                   $this->location['y'] === null ||
                   $this->aspect === null;

        if ($anyNull) {
            return false;
        } else {
            return true;
        }
    }

    public function place($x, $y, $aspectInString)
    {
        $tmpAspectIndex = array_search($aspectInString, $this->compass);

        if ($this->table->isValidXY($x, $y) && $tmpAspectIndex !== false) {
            $this->location['x'] = $x;
            $this->location['y'] = $y;

            $this->aspect = $tmpAspectIndex;
        } else {
            return;
        }
    }

    public function left()
    {
        if (!$this->hasBeenPlaced()) {
            return;
        } elseif ($this->table->isValidXY($this->location['x'], $this->location['y'])) {
            $this->aspect -= 1;

            if ($this->aspect < 0) {
                $this->aspect = 3;
            }
        } else {
            return;
        }
    }

    public function right()
    {
        if (!$this->hasBeenPlaced()) {
            return;
        } elseif ($this->table->isValidXY($this->location['x'], $this->location['y'])) {
            $this->aspect += 1;

            if ($this->aspect >= sizeof($this->compass)) {
                $this->aspect = 0;
            }
        } else {
            return;
        }
    }

    public function move()
    {
        if (!$this->hasBeenPlaced()) {
            return;
        }

        if ($this->aspect == array_search('NORTH', $this->compass)) {
            $newLocation = $this->getNewLocation(0, 1);

            if ($this->isValidSquare($newLocation)) {
                $this->location = $newLocation;
            } else {
                return;
            }
        } elseif ($this->aspect == array_search('EAST', $this->compass)) {
            $newLocation = $this->getNewLocation(1, 0);

            if ($this->isValidSquare($newLocation)) {
                $this->location = $newLocation;
            } else {
                return;
            }
        } elseif ($this->aspect == array_search('SOUTH', $this->compass)) {
            $newLocation = $this->getNewLocation(0, -1);

            if ($this->isValidSquare($newLocation)) {
                $this->location = $newLocation;
            } else {
                return;
            }
        } elseif ($this->aspect == array_search('WEST', $this->compass)) {
            $newLocation = $this->getNewLocation(-1, 0);

            if ($this->isValidSquare($newLocation)) {
                $this->location = $newLocation;
            } else {
                return;
            }
        } else {
            return;
        }
    }

    private function getNewLocation($xOffset, $yOffset)
    {
        $newLocation = [
            'x' => $this->location['x'] + $xOffset,
            'y' => $this->location['y'] + $yOffset,
        ];

        return $newLocation;
    }

    private function isValidSquare($newLocation)
    {
        $square = $this->table->getSquare(
            $newLocation['x'],
            $newLocation['y']
        );

        if ($square !== null) {
            return true;
        } else {
            return false;
        }
    }
}