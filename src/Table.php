<?php
namespace ToyRobot;

class Table
{
    private $table = null;

    public function __construct($xMax = 1, $yMax = 1)
    {
        $this->table =   array_fill(0, $xMax, array_fill(0, $yMax, new Square)
                            );
    }

    public function getXMax()
    {
        return sizeof($this->table);
    }

    public function getYMax()
    {
        return sizeof($this->table[0]);
    }

    public function getSquare($x, $y)
    {
        if ($this->isValidXY($x, $y)) {
            return $this->table[$x][$y];
        } else {
            return null;
        }
    }

    public function isValidXY($x, $y)
    {
        $isInt = is_int($x) && is_int($y);

        $withinRange =  $x >= 0 &&
                        $y >= 0 &&
                        $x < $this->getXMax() &&
                        $y < $this->getYMax();

        if ($isInt && $withinRange) {
            return true;
        } else {
            return false;
        }
    }
}