<?php
use PHPUnit\Framework\TestCase;
use ToyRobot\Command;

class CommandTest extends TestCase
{

    // tests
    public function testNew()
    {
        //test each command
        //test move
        $command = new Command('testdata/movefile');
        $this->assertEquals(['MOVE'], $command->getCommands());

        //test left
        $command = new Command('testdata/leftfile');
        $this->assertEquals(['LEFT'], $command->getCommands());

        //test right
        $command = new Command('testdata/rightfile');
        $this->assertEquals(['RIGHT'], $command->getCommands());

        //test report
        $command = new Command('testdata/reportfile');
        $this->assertEquals(['REPORT'], $command->getCommands());

        //test place
        $command = new Command('testdata/placefile');
        $this->assertEquals(['PLACE 0, 0, NORTH'], $command->getCommands());

        //test file with mixed commands
        $command = new Command('testdata/testfile1');
        $this->assertEquals([
            'MOVE',
            'LEFT',
            'RIGHT',
            'REPORT',
            'PLACE 0, 0, NORTH'
        ], $command->getCommands());

        //file with errors
        $command = new Command('testdata/testfile2');
        $this->assertInstanceOf(Command::class, $command);
        $this->assertNull($command->getCommands());
    }
}