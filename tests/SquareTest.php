<?php
use PHPUnit\Framework\TestCase;
use ToyRobot\Square;

class SquareTest extends TestCase
{

    // tests
    public function testNew()
    {
        $this->assertInstanceOf(Square::class, new Square);
    }

    public function testRobotCanOccupy()
    {
        $square = new Square;
        $this->assertTrue($square->robotCanOccupy());
    }

}