<?php
use PHPUnit\Framework\TestCase;
use ToyRobot\Table;
use ToyRobot\Square;

class TableTest extends TestCase
{

    /**
     * @var Table
     */
    protected $table;

    protected function setUp()
    {
        $this->table = new Table(5, 5);
    }

    // tests
    public function testNew()
    {
        $this->assertInstanceOf(Table::class, $this->table);
    }

    public function testGetXMax()
    {
        $this->assertEquals(5, $this->table->getXMax());
    }

    public function testGetYMax()
    {
        $this->assertEquals(5, $this->table->getYMax());
    }

    public function testGetSquare()
    {
        $this->assertInstanceOf(Square::class, $this->table->getSquare(2, 3));
    }

    public function testNegativeIndexGetSquare()
    {
        $this->assertNull($this->table->getSquare(-2, -3));
        $this->assertNull($this->table->getSquare(1, -3));
        $this->assertNull($this->table->getSquare(-2, 1));
    }
}